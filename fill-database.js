const MongoClient = require('mongodb').MongoClient;
const config = require("./config/params.js");

/**
 * Получаем массив параметров случайным образом.
 * @return {Array}
 */
function getRandomParams () {

  var max = 10;
  var min = 0;

  var probability = Math.round(Math.random() * (max - min) + min);
  var params = [];

  if (probability > 1) {
    params.push({name: "in", key: "i"})
  }
  if (probability > 4) {
    params.push({name: "equal", key: "e"})
  }
  if (probability > 8) {
    params.push({name: "join", key: "j"})
  }

  return params;
}

MongoClient.connect(config.db.getConnectionString(), (error, db) => {

  for (var i = 0; i < config.init.tasksCount; i++) {

    // Объявляем коллекцию
    var collection = db.collection("tasks");

    // Собираем данные для записи.
    var data = Object.assign({
      result: {},
      status: "none",
    }, {
      params: getRandomParams()
    });

    // Вставляем записи по очереди
    collection.insertOne(data, (err, result) => {

      console.log(err ? err : result);

      if (i === config.init.tasksCount) {
        console.log("Executed");
        db.close();
      }

    });
  }

})
