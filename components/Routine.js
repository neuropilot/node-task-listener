// Обертка над функциями Vagrant'a
const vagrant = require('node-vagrant');
const fs = require('fs');

// Специальная библиотечка для рекурсивого удаления всех директорий.
// Средствами Node получается громоздко
const rmdir = require('rimraf');

// Компонент для обратки событий
const EventEmitter = require("events").EventEmitter;

const config = require("../config/params.js");

const RedisStorage = require('./RedisStorage');
const RedisPublisher = require('redis').createClient();

// Библиотека для удобной обратки асинхронных функций. Тут она только из-за
// божественного waterfall
const async = require('async');


/**
 * Класс, реализующий основую работу с Vagrant машинами.
 * @param  {string} id Айдишник задачи, которую обрабатываем
 */
module.exports = function Routine (id) {

    this.id = id;

    // Здесь хранится результат выполнения программы внутри машины
    this.workerResult = null;

    // Экземпляр машины
    this.machine = null;

    // Экземпляр обработчика событий
    this.emitter = new EventEmitter();

    // Здесь нужно как то убивать уже запущенную машину, но вагрант не дает этого сделать
    this.forceKill = () => {
        if (!this.machine) {
            this.dir = `${__dirname}/../${config.machines.folder}/${this.id}`;

            if (!fs.existsSync(this.dir)) {
                this.dir += "_";
            }

            if (this.dir) {
                this.machine = vagrant.create({cwd: this.dir});
            } else {
                return;
            }
        }

        console.log(this.machine);

        /*async.waterfall([
            this.destroyMachine,
            this.removeDir,
        ]);*/
    };

    // Первичная инициализация машины
    this.init = () => {
        async.waterfall([
            /**
             * Читаем содержимое файла с конфигами
             * @param  {Function} callback
             */
            (callback) => {
                fs.readFile(`${__dirname}/../${config.machines.source}`, (err, content) => {
                    return callback(null, content.toString())
                })
            },

            /**
             * Подменяем переменные на наш айдишник, таким образом получаем
             * рабочий конфиг для Vagrant, и создаем директорию для машины
             * @param  {String}   content  Текст конфигов
             * @param  {Function} callback
             */
            (content, callback) => {

                this.vagrantConfig = content.replace(/{%name}/g, this.id);
                this.dir = `${__dirname}/../${config.machines.folder}/${this.id}`;

                if (fs.existsSync(this.dir)) {
                    this.dir += "_";
                }

                fs.mkdir(this.dir, (err) => {

                    if (err) {
                        return;
                    }

                    return callback();
                });
            },

            /**
             * Пишем полученные конфиги в Vagrantfile
             * @param  {Function} callback
             */
            (callback) => {
                this.filename = `${this.dir}/${config.machines.filename}`

                fs.writeFile(this.filename, this.vagrantConfig, (err) => {
                    if (err) {
                        throw err;
                    }

                    return callback();
                })
            },
            /**
             * Создаем объект машины и сообщаем о завершении
             * @param  {Function} callback
             */
            (callback) => {
                this.machine = vagrant.create({cwd: this.dir});
                this.emitter.emit("created");
            },
        ], (callback) => {return callback()});
    };

    /**
     * Функция для запуска машины и выполнения внутренней программы
     */
    this.run = () => {
        async.waterfall([
            /**
             * Запускаем машину
             * @param  {Function} callback
             */
            (callback) => {
                global.runnedCount++;
                this.machine.up((err, out) => {
                    if (err) {
                        this.executed = false;
                    }
                    console.log("machine is ready: " + this.id);

                    return callback();
                })
            },
            /**
             * Уведомляем об успехе запуска.
             * @param  {Function} callback
             */
            (callback) => {
                this.emitter.emit("ready")
            },
        ]);
    };

    /**
     * Считать данные из файла, в который был записан ответ программы
     */
    this.readResponse = () => {

        // Читаем файл
        fs.readFile(this.dir+"/result", (err, content) => {

            // Если внутри JSON - пишем его в результат
            try {
                this.workerResult = JSON.parse(content);
            // Может быть и не JSON (если программа не достучалась по роутам, или упал сам воркер)
            } catch (e) {
                // Пишем в ответ захардкоженый результат
                this.workerResult = {
                    status: false,
                    response: {
                        data: "Undefined error",
                    },
                }
            // В любом случае - уведомляем о готовности
            } finally {
                this.emitter.emit("read-response")
            }
        })
    };

    /**
     * Внести данные в Redis хранилище
     */
    this.put = () => {

        // Получаем список задач...
        RedisStorage.getItem(this.id, (item) => {

            // Присваиваем статус задаче
            if (this.workerResult.status) {
                item.status = "success";
            } else {
                item.status = "error";
            }

            item.response = this.workerResult.response;
            this.emitter.emit("putted", item);
        })
    };

    /**
     * Грохнуть экземпляр машины
     */
    this.die = () => {
        if (this.machine) {
            async.waterfall([
                // Останавливаем и убиваем машину
                this.destroyMachine,
                // Сносим директорию с машиной со всеми файлами
                this.removeDir,
                // Уведомляем об успехе
                (callback) => {
                    this.emitter.emit("dead")
                },
            ]);
        }

    };

    this.destroyMachine = (callback) => {
        global.runnedCount--;
        this.machine.destroy((err, out) => {
            if (err) {
                console.log(err);
            }
            return callback();
        });

    };

    this.removeDir = (callback) => {
        rmdir(this.dir, (err) => {
            if (err) {
                console.log(err);
                console.log(this.dir);
            }
            return callback()
        })
    };
}

/**
 * Получить текущую временную метку Unix
 * @return {int}
 */
function timestamp()
{
  return Math.floor(Date.now() / 1000);
}
